package common;

/**
 * Common static strings.
 */
public final class CommonStrings {

    /**
     * The window width.
     */
    public static final int WINDOW_WIDTH = 600;
    /**
     * The window height.
     */
    public static final int WINDOW_HEIGHT = 800;
    /**
     * The scaling factor for the window.
     */
    public static final double SCALING_FACTOR = 1.0;

    /**
     * Field separator, used for file reading.
     */
    public static final char FIELD_SEPARATOR = ';';
    /**
     * File separator, used for resources.
     */
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    /**
     * Path to the login file.
     */
    public static final String LOGIN_FILE = System.getProperty("user.dir") + FILE_SEPARATOR + "PlayersData.txt";
    /**
     * Path to the music file.
     */
    public static final String MUSIC_PATH = "resources" + FILE_SEPARATOR + "music" + FILE_SEPARATOR;

    /**
     * Key for the project.
     */
    public static final String KEY = "JumpManiaProject";

    private CommonStrings() {
    }

}
