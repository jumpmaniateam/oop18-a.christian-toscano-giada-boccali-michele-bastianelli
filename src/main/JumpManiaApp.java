package main;

import controller.menu.MainControllerImpl;
import javafx.application.Application;
import javafx.stage.Stage;
import view.SceneSwitcher;
import view.SceneSwitcherImpl;

/**
 * Application start point.
 */
public class JumpManiaApp extends Application {

    /**
     * Main.
     * 
     * @param args : args passed to the main method
     */
    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public final void start(final Stage primaryStage) throws Exception {
        System.setProperty("sun.java2d.opengl", "true");
        final SceneSwitcher switcher = new SceneSwitcherImpl(primaryStage);
        final MainControllerImpl startController = new MainControllerImpl(switcher);
        startController.start();
    }

}
