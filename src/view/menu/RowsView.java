package view.menu;

import java.util.List;

import common.CommonStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import view.ScoreView;

/**
 * Create a ScoreView with a title, the score and the button.
 */
public abstract class RowsView extends ScoreView {

    private static final Double ROWS = 2.0;

    private static final Double TOP = CommonStrings.WINDOW_WIDTH / 100.0 * 10;
    private static final Double BOTTOM = CommonStrings.WINDOW_WIDTH / 100.0 * 30;
    private static final Double LATERAL = 0.0;

    /**
     * RowsView constructor.
     * @param c : the constroller
     */
    public RowsView(final Controller c) {
        super(c);
        final GridPane pane = new GridPane();
        super.init(pane, TOP, BOTTOM, LATERAL, LATERAL);

        final RowConstraints titleRow = new RowConstraints();
        titleRow.setPercentHeight(super.getDimension().getHeight() / ROWS);
        final RowConstraints buttonRow = new RowConstraints();
        buttonRow.setPercentHeight(super.getDimension().getHeight() / ROWS);
        pane.getRowConstraints().addAll(titleRow, buttonRow);

        final VBox titleInfoBox = new VBox();
        final Label title = new Label(setTitle());
        title.setId("title");
        titleInfoBox.getChildren().addAll(title, super.getHBox());

        final VBox optionBtn = new VBox();
        optionBtn.getChildren().addAll(setButtons());

        int i = 0;
        pane.add(titleInfoBox, 0, i++);
        pane.add(optionBtn, 0, i++);
    }

    /**
     * Set title text.
     * @return the title String
     */
    public abstract String setTitle();

    /**
     * Add button to the view.
     * @return the list of button
     */
    public abstract List<Button> setButtons();

}
