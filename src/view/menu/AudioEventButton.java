package view.menu;

import common.MsgStrings;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import view.BackgroundMusic;
import view.GenericView;

/**
 * This class create a contrete AudioEventBusButton that send a MsgEvent on the
 * given view controller when is pressed. When is pressed, the button image is
 * changed.
 */
public class AudioEventButton extends AbstractEventButton {

    private static final Image FIRST = new Image("images/sound.png", 25, 25, true, true);
    private static final Image SECOND = new Image("images/mute.png", 25, 25, true, true);

    /**
     * Audio Button contstructor.
     * 
     * @param view : the view that create the concrete button
     */
    public AudioEventButton(final GenericView view) {
        super(view, MsgStrings.SWITCH_AUDIO_MODE);
        super.getButton().setGraphic(new ImageView(BackgroundMusic.getInstance().isOn() ? FIRST : SECOND));
    }

    private void chooseImageBtn() {
        super.getButton().setGraphic(new ImageView(BackgroundMusic.getInstance().isOn() ? SECOND : FIRST));
    }

    @Override
    public final void setAction() {
        this.chooseImageBtn();
    }
}
