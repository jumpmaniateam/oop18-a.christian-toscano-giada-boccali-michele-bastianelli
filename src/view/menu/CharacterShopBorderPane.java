package view.menu;

import java.util.Set;

import common.MsgStrings;
import enumerators.PlayerCharacter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import view.GenericView;

/**
 * Class that create a BorderPane with the single PlayerCharacter shop logic.
 */
public abstract class CharacterShopBorderPane extends BorderPane {

    private final Button buyBtn;
    private final Button selectBtn;
    private final PlayerCharacter character;
    private final GenericView view;

    /**
     * CharacterShopBorderPane constructor.
     * 
     * @param character : the character
     * @param view      : the view that create the concrete instance
     */
    public CharacterShopBorderPane(final PlayerCharacter character, final GenericView view) {
        super();
        this.character = character;
        this.view = view;

        final ImageView img = new ImageView(new Image("images/" + character.getImageName()));

        final HBox infoBox = new HBox();
        final Label coinLabel = new Label(String.valueOf(character.getCost()));

        buyBtn = createButton(MsgStrings.BUY_CHARACTER);
        buyBtn.setDisable(true);
        selectBtn = createButton(MsgStrings.SET_CHARACTER);
        selectBtn.setDisable(true);

        infoBox.getChildren().addAll(coinLabel, buyBtn, selectBtn);
        this.setCenter(img);
        this.setBottom(infoBox);
        this.setId("characters");
    }

    /**
     * Disable set button of current or locked characters, enable other Disable buy
     * button if character is unlocked.
     * 
     * @param data             : set of unlocked character
     * @param currentCharacter : the current character selected
     * @param coin             : actual coin owned
     */
    public void setButtonActivation(final Set<PlayerCharacter> data, final PlayerCharacter currentCharacter,
            final int coin) {
        if (data.contains(character)) {
            buyBtn.setDisable(true);
            if (character.equals(currentCharacter)) {
                selectBtn.setDisable(true);
            } else {
                selectBtn.setDisable(false);
            }
        } else {
            buyBtn.setDisable(false);
            selectBtn.setDisable(true);
        }
        this.checkTooExpensive(coin);
    }

    /**
     * Set buy button disable to active.
     * 
     * @param active : true if button is disable, false otherwise
     */
    public void setBuyActivation(final Boolean active) {
        buyBtn.setDisable(active);
    }

    /**
     * Set select button disable to active.
     * 
     * @param active : true if button is disable, false otherwise
     */
    public void setSelectActivation(final Boolean active) {
        selectBtn.setDisable(active);
    }

    /**
     * Set select button disable if there are enought coin.
     * 
     * @param coin : actual coin owned
     */
    public void checkTooExpensive(final int coin) {
        if (coin < this.character.getCost()) {
            buyBtn.setDisable(true);
        }
    }

    /**
     * Abstract method to be implemented to get character before button event is
     * send to the EventBus.
     */
    public abstract void selectCharacter();

    /**
     * Get the playerCharacter.
     * 
     * @return the player
     */
    public PlayerCharacter getCharacter() {
        return character;

    }

    private Button createButton(final String msg) {
        final Button b = new AbstractEventButton(view, msg) {
            @Override
            public void setAction() {
                selectCharacter();
            }
        }.getButton();
        b.setText(msg);
        return b;
    }
}
