package view.menu;

import java.util.ArrayList;
import java.util.List;

import common.MsgStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;

/**
 * Create a concrete end game view. Extends a GenericView.
 */
public class EndGameView extends RowsView {

    /**
     * EndGameView constructor.
     * 
     * @param c : EndGame controller
     */
    public EndGameView(final Controller c) {
        super(c);

    }

    @Override
    public final String setTitle() {
        return MsgStrings.END_GAME;
    }

    @Override
    public final List<Button> setButtons() {
        final List<Button> list = new ArrayList<Button>();
        list.add(new MsgEventButton(this, MsgStrings.MENU).getButton());
        list.add(new MsgEventButton(this, MsgStrings.RESTART).getButton());
        list.add(new AudioEventButton(this).getButton());
        return list;
    }

}
