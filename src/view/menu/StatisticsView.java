package view.menu;

import java.util.ArrayList;
import java.util.List;

import common.CommonStrings;
import common.MsgStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.TilePane;
import view.GenericViewImpl;

/**
 * Create a concrete statistic view. Extends a GenericView.
 */
public class StatisticsView extends GenericViewImpl {

    private static final Double TOP = CommonStrings.WINDOW_WIDTH / 100.0 * 10;
    private static final Double BOTTOM = CommonStrings.WINDOW_WIDTH / 100.0 * 10;
    private static final Double LATERAL = 0.0;

    /**
     * StatisticView constructor.
     * 
     * @param c        : Statistic controller.
     * @param infoList : list with the info to display
     */
    public StatisticsView(final Controller c, final List<String> infoList) {
        super(c);
        final TilePane box = new TilePane();
        super.init(box, TOP, BOTTOM, LATERAL, LATERAL);

        final List<Label> listStat = new ArrayList<Label>(getStat(infoList));

        final Button menuBtn = new MsgEventButton(this, MsgStrings.MENU).getButton();

        box.getChildren().addAll(listStat);
        box.getChildren().add(menuBtn);
    }

    private List<Label> getStat(final List<String> infoList) {
        final List<Label> list = new ArrayList<>();
        infoList.forEach(e -> {
            final Label l = new Label(e);
            list.add(l);
        });
        return list;
    }

}
