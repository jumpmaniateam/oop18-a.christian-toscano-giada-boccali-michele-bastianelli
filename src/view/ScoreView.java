package view;

import controller.menu.Controller;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * Create a generic view with method to handle a CoinScoreInfo instance.
 */
public class ScoreView extends GenericViewImpl {

    private static final String SCORE = "Score: ";
    private static final String COIN = "Coin: ";

    private final Label score;
    private final Label coin;

    /**
     * ScoreView constructor.
     * @param c : the controller
     */
    public ScoreView(final Controller c) {
        super(c);
        score = new Label("0");
        coin = new Label("0");
    }

    /**
     * CoinScoreInfo info setter.
     * @param coin : coins
     * @param score : max score
     */
    public void setInfo(final int coin, final int score) {
        this.setCoin(coin);
        this.setScore(score);
    }

    /**
     * Return an HBox with the information.
     * 
     * @return an HBox with the information.
     */
    public HBox getHBox() {
        final HBox infoBox = new HBox();
        infoBox.getChildren().addAll(score, coin);
        return infoBox;
    }

    /**
     * Method to set player score.
     * 
     * @param score : score to set
     */
    private void setScore(final int score) {
        this.score.setText(SCORE + score);
    }

    /**
     * Method to set player coin.
     * 
     * @param coin : coin to set
     */
    private void setCoin(final int coin) {
        this.coin.setText(COIN + coin);
    }


}
