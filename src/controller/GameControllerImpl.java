package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jbox2d.common.Vec2;

import com.google.common.eventbus.Subscribe;

import common.CommonStrings;
import common.MsgStrings;
import common.SceneEventBusConnection;
import common.events.CoinEvent;
import common.events.EscEvent;
import enumerators.Level;
import enumerators.PlayerCharacter;
import enumerators.SceneType;
import model.GameModel;
import model.GameModelImpl;
import model.user.UserModelImpl;
import utils.Box2DUtils;
import utils.FileUserManager;
import view.GameView;
import view.GameViewImpl;
import view.GenericView;
import view.PlayerInputManager;

/**
 * The game main controller.
 */
public final class GameControllerImpl extends SceneEventBusConnection implements GameController {

    private static final GameControllerImpl INSTANCE = new GameControllerImpl();

    private static final Vec2 PLAYER_POSITION = new Vec2(CommonStrings.WINDOW_WIDTH / 2,
            (float) (CommonStrings.WINDOW_HEIGHT * 0.1));
    private static final long FPS = 60;
    private final UserModelImpl userModel = new UserModelImpl();

    /**
     * Height the player has to reach to win.
     */
    public static final double VICTORY_HEIGHT = 15000f;

    private List<Generator> generators;
    private PlayerInputManager input;
    private HUD hud;
    private GameModel model;
    private GameView view;

    private int step;
    private Level level;
    private boolean victory;
    private boolean firstTime;

    private GameControllerImpl() {
        super();
        firstTime = true;
    }

    /**
     * Returns the singleton instance of the GameController.
     * 
     * @return instance
     */
    public static GameControllerImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public void initNewGame(final PlayerCharacter playerCharacter, final Optional<Level> optionalLevel) {
        if (optionalLevel.isPresent()) {
            this.level = optionalLevel.get();
            if (firstTime) {
                view = new GameViewImpl(FPS, this);
                model = new GameModelImpl(FPS);
                hud = new HUDImpl();
                input = new PlayerInputManager();
                generators = new ArrayList<>();
                generators.add(new PlatformGenerator());
                generators.add(new EnemyGenerator());
                generators.add(new CoinGenerator());
                firstTime = false;
            }
            view.init(level);
            model.init(level);
            model.createPlayer(playerCharacter, PLAYER_POSITION);
            hud.init(0, 0);
            input.addListeners(view.getScene());
            generators.forEach(g -> g.init(level, model));
            victory = false;
            view.timerStart();
        } else {
            throw new IllegalArgumentException("Null level exception");
        }
    }

    @Override
    public void tick() {
        step++;
        model.updateWorld();
        view.getCamera().update();
        model.updateEntities();
        generators.forEach(g -> g.update());

        final double currentPhyY = Box2DUtils.box2DYToJavaFX(model.getPlayer().getPhysicPosition().y);
        checkVictory(currentPhyY);

        hud.setMaxHeight((int) Math.round(currentPhyY));
        this.view.setInfo(hud.getTotalCoins(), hud.getMaxHeight());
        model.checkEntitiesToDelete();
        if (model.isPlayerDead()) {
            gameOver();
           this.postScene(SceneType.END_GAME);
        }
    }

    @Override
    public void resumeGame() {
        view.timerStart();
        input.addListeners(view.getScene());
    }

    /**
     * Pauses the game.
     */
    public void pauseGame() {
        view.timerStop();
        input.removeListeners(view.getScene());
        userModel.getUser().addCoin(hud.getCoins());
        userModel.getUser().setMaxHeight(hud.getMaxHeight());
        hud.resetTempCoins();
    }

    /**
     * Handles game over. Resets world, stops the timer and removes the input
     * listeners. It also saves the score.
     */
    public void gameOver() {
        view.timerStop();
        model.resetWorld();
        input.removeListeners(view.getScene());
        userModel.getUser().addCoin(hud.getCoins());
        userModel.getUser().setMaxHeight(hud.getMaxHeight());
        FileUserManager.update(userModel.getUser());
    }

    private void checkVictory(final double currentY) {
        if (currentY >= VICTORY_HEIGHT && !victory) {
            userModel.getUser().unlockNewLevel(level);
            victory = true;
            this.pauseGame();
            this.postScene(SceneType.WIN_GAME);
        }
    }

    @Override
    public int getStep() {
        return step;
    }

    @Override
    public GameView getGameView() {
        return this.view;
    }

    @Override
    public GameModel getGameModel() {
        return model;
    }

    @Override
    public Level getLevel() {
        return level;
    }

    /**
     * Returns if the player has won.
     * @return <code>true</code> if the player has won
     */
    public boolean isLevelPassed() {
        return victory;
    }

    @Override
    @Deprecated
    public void setLevel(final Level level) {
        this.level = level;
    }

    @Override
    public GenericView getView() {
        return view;
    }

    @Override
    public void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.END_GAME:
            gameOver();
            this.postScene(SceneType.END_GAME);
            break;
        case MsgStrings.PAUSE:
            break;
        default:
            break;
        }
    }

    @Override
    public void handleKeyEvent(final EscEvent event) {
        this.pauseGame();
        this.postScene(SceneType.GAME_MENU);
    }

    /**
     * Handle the coin event and adds the coin value to the score.
     * 
     * @param event : the CoinEvent
     */
    @Subscribe
    public void handleCoinEvent(final CoinEvent event) {
        this.hud.addCoins(event.getCoinValue());
    }

}
