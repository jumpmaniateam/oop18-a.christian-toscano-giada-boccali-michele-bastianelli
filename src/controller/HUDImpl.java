package controller;

/**
 * HUD implementation. Must be initialized.
 */
public class HUDImpl implements HUD {

    private int coins;
    private int totalCoins;
    private int maxHeight;

    @Override
    public final void init(final int maxHeight, final int coins) {
        this.maxHeight = maxHeight;
        this.coins = coins;
        this.totalCoins = coins;
    }

    @Override
    public final void addCoins(final int coins) {
        this.coins += coins;
        this.totalCoins += coins;
    }

    @Override
    public final void removeCoins(final int coins) {
        this.coins -= coins;
        this.totalCoins -= coins;
    }

    @Override
    public final int getCoins() {
        return this.coins;
    }

    @Override
    public final void resetTempCoins() {
        this.coins = 0;
    }

    @Override
    public final int getTotalCoins() {
        return totalCoins;
    }

    @Override
    public final int getMaxHeight() {
        return maxHeight;
    }

    @Override
    public final void setMaxHeight(final int maxHeight) {
        if (maxHeight > this.maxHeight) {
            this.maxHeight = maxHeight;
        }
    }

}
