package controller.entities;

import common.events.JumpEvent;

/**
 * Give the possibility to jump when the linked events is received.
 */
public interface Jumper {

    /**
     * Subscribe to the JumpEvent.
     * @param event the event received when the entity has to jump.
     */
    void handleJumpEvent(JumpEvent event);
}
