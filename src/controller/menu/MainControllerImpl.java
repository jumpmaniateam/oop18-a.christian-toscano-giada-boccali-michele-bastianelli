package controller.menu;

import common.SceneEventBusConnection;
import common.events.SceneEvent;
import controller.GameControllerImpl;
import enumerators.FileAudio;
import enumerators.SceneType;
import model.user.UserModel;
import model.user.UserModelImpl;
import view.BackgroundMusic;
import view.SceneSwitcher;

/**
 * MainController implementation. This class handle SceneEvent. Set the correct
 * scene as Stage scene
 */
public class MainControllerImpl extends SceneEventBusConnection implements MainController {

    private final SceneSwitcher sceneSwitcher;
    private Controller menu;
    private final UserModel userModel;

    /**
     * Create the mail controller and call EventBusConnection constructor.
     * 
     * @param switcher : the application stage
     */
    public MainControllerImpl(final SceneSwitcher switcher) {
        super();
        this.sceneSwitcher = switcher;
        userModel = new UserModelImpl();
    }

    @Override
    public final void handleSceneEvent(final SceneEvent event) {
        switch (event.getSceneType()) {
        case LOGIN:
            this.setMenu(new LoginMenu());
            break;
        case MENU:
            this.setMenu(new MainMenu());
            break;
        case NEW_GAME:
            GameControllerImpl.getInstance().initNewGame(
                    userModel.getUser().getCurrentCharacter(),
                    userModel.getCurrentLevel());
            this.setMenu(GameControllerImpl.getInstance());
            break;
        case RESUME_GAME:
            this.setMenu(GameControllerImpl.getInstance());
            GameControllerImpl.getInstance().resumeGame();
            break;
        case GAME_MENU:
            this.setMenu(new PauseMenu());
            break;
        case STATISTICS:
            this.setMenu(new StatisticMenu());
            break;
        case SHOP:
            this.setMenu(new ShopMenu());
            break;
        case END_GAME:
            this.setMenu(new EndGameMenu());
            break;
        case WIN_GAME:
            this.setMenu(new WinMenu());
            break;
        default:
            throw new IllegalAccessError("Scene not present");
        }
        sceneSwitcher.setScene(this.menu.getView());
    }

    @Override
    public final void start() {
        BackgroundMusic.getInstance().loadTune(FileAudio.TRACK_1.getPath());
        this.postScene(SceneType.LOGIN);
    }

    private void setMenu(final Controller menu) {
        this.menu = menu;
    }

}
