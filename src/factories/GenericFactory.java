package factories;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import org.jbox2d.common.Vec2;

import controller.GameControllerImpl;
import enumerators.SpecificType;
import model.entities.EntityModel;
import view.GameView;
import view.entities.EntityView;
import view.entities.EntityViewImpl;

/**
 * Abstract base factory. This should not be instantiated.
 */
abstract class GenericFactory<K extends SpecificType, V extends EntityModel> {

    private static Optional<Vec2> pos;

    /**
     * @return the position where to create the entity
     */
    protected static Vec2 getPos() {
        return pos.get();
    }

    /**
     * Create a new entity model.
     * 
     * @param K        the entity specific type
     * @param V        the entity model constructor
     * @param type     the entity specific type
     * @param position the position where to create the entity
     * @param map      the map where K is the entity specific type and Supplier<V>
     *                 is the supplier that create the new entity model
     * @return
     */
    protected V createEntityModel(final K type, final Vec2 position, final Map<K, Supplier<V>> map) {
        pos = Optional.ofNullable(position);
        if (pos.isPresent() && map.containsKey(type)) {
            return map.get(type).get();
        } else if (!pos.isPresent()) {
            throw new IllegalArgumentException("Position to create the entity is not valid!");
        } else {
            throw new IllegalArgumentException("Character not found: " + type.toString());
        }
    }

    /**
     * @param eModel the {@link EntityModel} of the entity to create
     * @return the entity view
     */
    protected static EntityView createView(final EntityModel eModel) {
        final GameView gView = GameControllerImpl.getInstance().getGameView();
        final EntityView view = new EntityViewImpl(eModel, eModel.getSpecificType(), gView);
        view.fitViewToDimension(false);
        return view;
    }

    /**
     * Throw an exception for a bad entity character.
     * 
     * @param msg
     */
    protected static void throwErrorBadCharacter(final String msg) {
        throw new IllegalArgumentException("Character not found: " + msg);
    }

    /**
     * Throw an exception for a bad Class type.
     * 
     * @param msg
     */
    protected static void throwErrorBadClass(final Class<?> type) {
        throw new IllegalArgumentException("Class type not valid: " + type.toString());
    }
}
