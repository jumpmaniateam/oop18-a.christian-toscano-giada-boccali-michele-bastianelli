package factories;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.jbox2d.common.Vec2;

import controller.entities.Player;
import enumerators.PlayerCharacter;
import model.components.AttackImpl;
import model.components.ComandableMovement;
import model.components.JumpImpl;
import model.entities.PlayerModel;
import model.physics.BodyBuilderImpl;
import model.physics.Size2D;
import view.entities.EntityView;

/**
 * Factory to generate the {@link Player}.
 */
public final class FactoryPlayer extends GenericFactory<PlayerCharacter, PlayerModel> {

    private static final Map<PlayerCharacter, Supplier<PlayerModel>> PLAYER_MAP = new HashMap<>();
    static {
        PLAYER_MAP.put(PlayerCharacter.BIRD, () -> new Bird(getPos()));
        PLAYER_MAP.put(PlayerCharacter.SHEEP, () -> new Sheep(getPos()));
        PLAYER_MAP.put(PlayerCharacter.TUX, () -> new Tux(getPos()));
    }

    /**
     * Create a player.
     * 
     * @param type     the player type
     * @param position the position where to place
     * @return the player controller
     */
    protected Player createPlayer(final PlayerCharacter type, final Vec2 position) {
        final PlayerModel model = createPlayerModel(type, position);
        final EntityView view = createView(model);
        return new Player(model, view);
    }

    /**
     * Create a player model.
     * 
     * @param type the player character
     * @param position the position where to place
     * @return the player model
     */
    protected PlayerModel createPlayerModel(final PlayerCharacter type, final Vec2 position) {
        return createEntityModel(type, position, PLAYER_MAP);
    }

    /**
     * Bird player character.
     */
    private static final class Bird extends PlayerModel {
        private static final PlayerCharacter TYPE = PlayerCharacter.BIRD;

        Bird(final Vec2 position) {
            super(TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(TYPE.getWidth(), TYPE.getHeight()))
                            .moveable(true)
                            .solid(true)
                            .allowedToSleep(false)
                            .friction(0)
                            .density(1)
                            .restitution(0)
                            .gravity(1)
                            .build());
            this.addDefaultPlayerJump();
            this.addDefaultPlayerMovement();
            this.addDefaultPlayerAttack();
            this.addDefaultPlayerLife();
            this.addDefaultPlayerCollisionEffect();
        }
    }

    /**
     * Sheep player character.
     */
    private static final class Sheep extends PlayerModel {
        private static final PlayerCharacter TYPE = PlayerCharacter.SHEEP;

        private static final float JUMP = 80.0f;
        private static final float SPEED = 2.1f;
        private static final float GRAVITY = 0.9f;

        Sheep(final Vec2 position) {
            super(TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(TYPE.getWidth(), TYPE.getHeight()))
                            .moveable(true)
                            .density(1)
                            .allowedToSleep(false)
                            .friction(0)
                            .restitution(0)
                            .gravity(GRAVITY)
                            .build());
            this.addDefaultPlayerAttack();
            this.addDefaultPlayerLife();
            this.add(new ComandableMovement(this, SPEED));
            this.add(new JumpImpl(this, JUMP));
            this.addDefaultPlayerCollisionEffect();
        }
    }

    /**
     * Tux player character.
     */
    private static final class Tux extends PlayerModel {
        private static final PlayerCharacter TYPE = PlayerCharacter.TUX;

        private static final float JUMP = 85.0f;
        private static final float SPEED = 2.2f;
        private static final float GRAVITY = 0.8f;
        private static final int ATTACK = 2;

        Tux(final Vec2 position) {
            super(TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(TYPE.getWidth(), TYPE.getHeight()))
                            .moveable(true)
                            .density(1)
                            .allowedToSleep(false)
                            .friction(0)
                            .restitution(0)
                            .gravity(GRAVITY)
                            .build());
            this.addDefaultPlayerLife();
            this.add(new AttackImpl(ATTACK));
            this.add(new ComandableMovement(this, SPEED));
            this.add(new JumpImpl(this, JUMP));
            this.addDefaultPlayerCollisionEffect();
        }
    }

}
