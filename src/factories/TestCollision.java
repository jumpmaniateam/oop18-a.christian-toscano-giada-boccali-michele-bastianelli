package factories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.jbox2d.common.Vec2;
import org.junit.Before;
import org.junit.Test;

import enumerators.EnemyCharacter;
import enumerators.Level;
import enumerators.PlatformType;
import enumerators.PlayerCharacter;
import model.CollisionHandler.CollisionSide;
import model.GameModel;
import model.GameModelImpl;
import model.components.CollisionImpl;
import model.entities.EnemyModel;
import model.entities.PlatformModel;
import model.entities.PlayerModel;

/**
 * Collision Test.
 */
public class TestCollision {

    private static final int POS_Y = 150;
    private static final String TUX_DEAD = "Tux dead";
    private static final long FPS = 60;
    private final GameModel gameModel = new GameModelImpl(FPS);
    private final FactoryPlayer fp = new FactoryPlayer();
    private final FactoryPlatform fpp = new FactoryPlatform();
    private final FactoryEnemy fEnemy = new FactoryEnemy();
    private PlayerModel tux;

    /**
     * Set up model and player.
     */
    @Before
    public void setUp() {
        gameModel.init(Level.LEVEL_1);
        tux = fp.createPlayerModel(PlayerCharacter.TUX, new Vec2(100, 100));
    }

    /**
     * Test platform.
     */
    @Test
    public void test1() {
        assertNotNull("Tux null", tux);
        final PlatformModel plat = fpp.createPlatformModel(PlatformType.ONEJUMP, new Vec2(150, 250));
        assertNotNull("Plarform null", plat);

        tux.getComponent(CollisionImpl.class).applyCollisionEffect(plat, CollisionSide.BOTTOM);
        plat.getComponent(CollisionImpl.class).applyCollisionEffect(tux, CollisionSide.BOTTOM);
        // platform should die after one jump
        assertFalse("Plad alive", plat.isAlive());
        assertTrue(TUX_DEAD, tux.isAlive());
        assertTrue("Tux wrong position", tux.getPhysicPosition().y <= POS_Y);
    }

    /**
     * Test platform collision.
     */
    @Test
    public void test2() {
        PlatformModel plat = fpp.createPlatformModel(PlatformType.ONEJUMP, new Vec2(150, 250));
        assertNotNull("Platform null", plat);

        tux.getComponent(CollisionImpl.class).applyCollisionEffect(plat, CollisionSide.OTHERS);
        plat.getComponent(CollisionImpl.class).applyCollisionEffect(tux, CollisionSide.OTHERS);
        // platform should not die
        assertTrue("Platform not alive", plat.isAlive());
        assertTrue(TUX_DEAD, tux.isAlive());
    }

    /**
     * Test frosty enemy and bottom collision.
     */
    @Test
    public void test3() {
        final EnemyModel enemy = fEnemy.createEnemyModel(EnemyCharacter.FROSTY, new Vec2(100, 100));
        assertNotNull("Enemy null", enemy);
        tux.getComponent(CollisionImpl.class).applyCollisionEffect(enemy, CollisionSide.BOTTOM);
        enemy.getComponent(CollisionImpl.class).applyCollisionEffect(tux, CollisionSide.BOTTOM);
        assertFalse("Enemy alive", enemy.isAlive());
        assertTrue(TUX_DEAD, tux.isAlive());
    }

    /**
     * Test frosty enemy and not bottom collision.
     */
    @Test
    public void test4() {
        final EnemyModel enemy = fEnemy.createEnemyModel(EnemyCharacter.FROSTY, new Vec2(100, 100));
        assertNotNull("Enemy null", enemy);
        tux.getComponent(CollisionImpl.class).applyCollisionEffect(enemy, CollisionSide.OTHERS);
        enemy.getComponent(CollisionImpl.class).applyCollisionEffect(tux, CollisionSide.OTHERS);
        assertTrue("Enemy dead", enemy.isAlive());
        assertFalse("Tux alive", tux.isAlive());
    }

    /**
     * Test two enemy collision.
     */
    @Test
    public void test5() {
        final EnemyModel enemy1 = fEnemy.createEnemyModel(EnemyCharacter.FROSTY, new Vec2(100, 100));
        final EnemyModel enemy2 = fEnemy.createEnemyModel(EnemyCharacter.FROSTY, new Vec2(100, 100));

        enemy1.getComponent(CollisionImpl.class).applyCollisionEffect(enemy2, CollisionSide.BOTTOM);
        enemy2.getComponent(CollisionImpl.class).applyCollisionEffect(enemy1, CollisionSide.OTHERS);
        assertTrue("Enemy1 dead", enemy1.isAlive());
        assertTrue("Enemy2 dead", enemy2.isAlive());
    }

    /**
     * Test bomb enemy and bottom collision.
     */
    @Test
    public void test6() {
        final EnemyModel bomb = fEnemy.createEnemyModel(EnemyCharacter.BOMB, new Vec2(100, 100));
        tux.getComponent(CollisionImpl.class).applyCollisionEffect(bomb, CollisionSide.BOTTOM);
        assertTrue(TUX_DEAD, tux.isAlive());
        assertFalse("Bomb alive", bomb.isAlive());

        bomb.getComponent(CollisionImpl.class).applyCollisionEffect(tux, CollisionSide.BOTTOM);
        assertFalse("Tux alive", tux.isAlive());
    }
}
