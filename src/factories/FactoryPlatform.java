package factories;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.jbox2d.common.Vec2;

import controller.entities.Platform;
import enumerators.EntityType;
import enumerators.PlatformType;
import model.CollisionHandler.CollisionSide;
import model.components.CollisionImpl;
import model.components.JumpImpl;
import model.components.LifeImpl;
import model.entities.PlatformModel;
import model.physics.BodyBuilderImpl;
import model.physics.Size2D;
import view.entities.EntityView;

/**
 * Factory to generate a new Platform.
 */
public class FactoryPlatform extends GenericFactory<PlatformType, PlatformModel> {

    private static final Map<PlatformType, Supplier<PlatformModel>> PLATFORM_MAP = new HashMap<>();
    static {
        PLATFORM_MAP.put(PlatformType.SIMPLE, () -> new SimplePlatform(getPos()));
        PLATFORM_MAP.put(PlatformType.SUPERJUMP, () -> new SuperJumpPlatform(getPos()));
        PLATFORM_MAP.put(PlatformType.ONEJUMP, () -> new OneJumpPlatform(getPos()));
    }

    /**
     * Create a platform.
     * @param type the platform type
     * @param position the position where to place
     * @return the platform controller
     */
    protected final Platform createPlatform(final PlatformType type, final Vec2 position) {
        final PlatformModel model = createPlatformModel(type, position);
        final EntityView view = createView(model);
        return new Platform(model, view);
    }

    /**
     * Create a platform model.
     * @param type the platform character
     * @param position the position where to place
     * @return the platform model
     */
    public final PlatformModel createPlatformModel(final PlatformType type, final Vec2 position) {
        return createEntityModel(type, position, PLATFORM_MAP);
    }

    /**
     * Simple static platform.
     */
    private static final class SimplePlatform extends PlatformModel {

        private static final PlatformType P_TYPE = PlatformType.SIMPLE;

        SimplePlatform(final Vec2 position) {
            super(P_TYPE,
                    BodyBuilderImpl.getInstance()
                                    .size(new Size2D(P_TYPE.getWidth(), P_TYPE.getHeight()))
                                    .position(position)
                                    .allowedToSleep(true)
                                    .friction(0)
                                    .solid(true)
                                    .moveable(false)
                                    .restitution(0)
                                    .build());
            this.add(new CollisionImpl((e, s) -> {
                if (e.getEntityType().equals(EntityType.PLAYER)) {
                    e.getComponent(JumpImpl.class).jump();
                }
            }));
        }
    }

    /**
     * A platform that give a super jump.
     */
    private static final class SuperJumpPlatform extends PlatformModel {

        private static final PlatformType P_TYPE = PlatformType.SUPERJUMP;

        SuperJumpPlatform(final Vec2 position) {
            super(P_TYPE, BodyBuilderImpl.getInstance()
                                    .size(new Size2D(P_TYPE.getWidth(), P_TYPE.getHeight()))
                                    .position(position)
                                    .allowedToSleep(true)
                                    .friction(0)
                                    .solid(true)
                                    .moveable(false)
                                    .restitution(0)
                                    .build());
            this.add(new CollisionImpl((e, s) -> {
                if (e.getEntityType().equals(EntityType.PLAYER)) {
                    e.getComponent(JumpImpl.class).jumpFromExternalForce(100);
                }
            }));
        }
    }

    /**
     * A platform that will disappears after one jump.
     */
    private static final class OneJumpPlatform extends PlatformModel {

        private static final PlatformType P_TYPE = PlatformType.ONEJUMP;
        private static final int LIFE = 1;

        OneJumpPlatform(final Vec2 position) {
            super(P_TYPE, BodyBuilderImpl.getInstance()
                    .size(new Size2D(P_TYPE.getWidth(), P_TYPE.getHeight()))
                    .position(position)
                    .allowedToSleep(true)
                    .friction(0)
                    .solid(true)
                    .moveable(false)
                    .restitution(0)
                    .build());
            this.add(new LifeImpl(this, LIFE, LIFE, LIFE));
            this.add(new CollisionImpl((e, s) -> {
                if (e.getEntityType().equals(EntityType.PLAYER) && s.equals(CollisionSide.BOTTOM)) {
                    e.getComponent(JumpImpl.class).jump();
                    if (this.contain(LifeImpl.class)) {
                        // make the platform disappears after one jump
                        this.getComponent(LifeImpl.class).setDead();
                    }
                }
            }));
        }
    }

}
