package factories;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.jbox2d.common.Vec2;

import controller.entities.Enemy;
import enumerators.EnemyCharacter;
import enumerators.EntityType;
import model.components.AttackImpl;
import model.components.CoinValueImpl;
import model.components.CollisionImpl;
import model.entities.EnemyModel;
import model.physics.BodyBuilderImpl;
import model.physics.Size2D;
import view.entities.EntityView;

/**
 * Factory to generate a new enemy.
 */
public class FactoryEnemy extends GenericFactory<EnemyCharacter, EnemyModel> {

    /**
     * Contain all the enemy models.
     */
    private static final Map<EnemyCharacter, Supplier<EnemyModel>> ENEMY_MAP = new HashMap<>();
    static {
        ENEMY_MAP.put(EnemyCharacter.PARABEETLE, () -> new Parabeetle(getPos()));
        ENEMY_MAP.put(EnemyCharacter.GOOMBA, () -> new Goomba(getPos()));
        ENEMY_MAP.put(EnemyCharacter.BOMB, () -> new Bomb(getPos()));
        ENEMY_MAP.put(EnemyCharacter.FROSTY, () -> new Frosty(getPos()));
    }

    /**
     * Create an enemy.
     * @param type the enemy type
     * @param position the position where to place
     * @return the enemy controller
     */
    protected final Enemy createEnemy(final EnemyCharacter type, final Vec2 position) {
        final EnemyModel model = createEnemyModel(type, position);
        final EntityView view = createView(model);
        return new Enemy(model, view);
    }

    /**
     * Create a enemy model.
     * @param type the enemy character
     * @param position the position where to place
     * @return the enemy model
     */
    protected final EnemyModel createEnemyModel(final EnemyCharacter type, final Vec2 position) {
        return createEntityModel(type, position, ENEMY_MAP);
    }

    /**
     * Parabeetle model class. Specialization of the EnemyModel class
     */
    private static final class Parabeetle extends EnemyModel {

        private static final EnemyCharacter E_TYPE = EnemyCharacter.PARABEETLE;

        Parabeetle(final Vec2 position) {
            super(E_TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(E_TYPE.getWidth(), E_TYPE.getHeight()))
                            .moveable(false)
                            .solid(true)
                            .restitution(0)
                            .gravity(0)
                            .build());
            this.addDefaultEnemyLife();
            this.addDefaultEnemyCollisionEffect();
            this.addDefaultEnemyAttack();
        }

    }

    /**
     * Goomba model class. Specialization of the EnemyModel class
     */
    private static final class Goomba extends EnemyModel {

        private static final EnemyCharacter E_TYPE = EnemyCharacter.GOOMBA;

        Goomba(final Vec2 position) {
            super(E_TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(E_TYPE.getWidth(), E_TYPE.getHeight()))
                            .moveable(false)
                            .solid(true)
                            .restitution(0)
                            .gravity(0)
                            .build());
            this.addDefaultEnemyLife();
            this.addDefaultEnemyCollisionEffect();
            this.addDefaultEnemyAttack();
        }
    }

    /**
     * Bomb model class. Specialization of the EnemyModel class
     */
    private static final class Bomb extends EnemyModel {

        private static final EnemyCharacter E_TYPE = EnemyCharacter.BOMB;
        private static final int COIN_VALUE = 1;

        Bomb(final Vec2 position) {
            super(E_TYPE, BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(E_TYPE.getWidth(), E_TYPE.getHeight()))
                            .moveable(false)
                            .solid(true)
                            .restitution(0)
                            .gravity(0)
                            .build());
            this.addDefaultEnemyLife();
            this.addDefaultEnemyAttack();
            this.add(new CoinValueImpl(COIN_VALUE));
            this.add(new CollisionImpl((e, s) -> {
                if (e.getEntityType().equals(EntityType.PLAYER) && this.contain(AttackImpl.class)) {
                    this.getComponent(AttackImpl.class).applyDamage(e);
                }
            }));
        }

    }

    /**
     * Frosty model class. Specialization of the EnemyModel class
     */
    private static final class Frosty extends EnemyModel {

        private static final EnemyCharacter E_TYPE = EnemyCharacter.FROSTY;
        private static final int ATTACK_VALUE = 2;
        private static final int COIN_VALUE = 2;

        Frosty(final Vec2 position) {
            super(E_TYPE,
                    BodyBuilderImpl.getInstance()
                            .position(position)
                            .size(new Size2D(E_TYPE.getWidth(), E_TYPE.getHeight()))
                            .moveable(false)
                            .solid(true)
                            .restitution(0)
                            .gravity(0)
                            .build());
            this.addDefaultEnemyLife();
            this.addDefaultEnemyCollisionEffect();
            this.add(new CoinValueImpl(COIN_VALUE));
            this.add(new AttackImpl(ATTACK_VALUE));
        }
    }

}
