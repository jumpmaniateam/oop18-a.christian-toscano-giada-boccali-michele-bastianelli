package test;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import view.BackgroundMusic;

/**
 *
 */
public class MusicTest {

    /**
     * 
     */
    @Test
    public void testMusic() {
        BackgroundMusic.getInstance().loadTune("aaa");
        assertFalse("Music is on", BackgroundMusic.getInstance().isOn());
        BackgroundMusic.getInstance().switchAudioMode();
        BackgroundMusic.getInstance().stop();
        BackgroundMusic.getInstance().start();
        assertFalse("Music is on", BackgroundMusic.getInstance().isOn());
    }
}
