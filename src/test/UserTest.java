package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import enumerators.Level;
import enumerators.PlayerCharacter;
import model.user.CurrentUserImpl;
import model.user.UserImpl;
import model.user.UserModelImpl;
import model.user.UserProfile;
import model.user.UserProfileImpl;
import utils.FileUserManager;

/**
 * Check CurrentUser values.
 */
public class UserTest {

    private UserProfile userProfile;
    private UserModelImpl model;

    /**
     * Creation of an user profile.
     */
    @Before
    public void init() {
        userProfile = new UserProfileImpl("Admin", "1234");
        model = new UserModelImpl();
    }

    /**
     * Test methods of the current user initialization passing by UserModel.
     */
    @Test
    public void testCurrentUserCreation() {
        assertNotNull("not initialized user", CurrentUserImpl.getInstance().getUser());
        assertEquals("Reference error", model.getUser(), CurrentUserImpl.getInstance().getUser());
    }

    /**
     * Creation User and initial values test.
     */
    @Test
    public void testNewProfile() {
        CurrentUserImpl.getInstance().setUser(new UserImpl(userProfile));
        assertTrue("Username don't match", model.getUser().getUserProfile().getUsername().equals(userProfile.getUsername()));
        assertTrue("Password don't match", model.getUser().getUserProfile().getPassword().equals(userProfile.getPassword()));
        assertSame("Wrong init height", model.getUser().getMaxHeight(), 0);
        model.setNextCurrentLevel();
        assertTrue("Wrong init Level", model.getUser().getMaxLevel().equals(Level.LEVEL_1));
        assertTrue("Wrong init character", model.getUser().getCurrentCharacter().equals(PlayerCharacter.values()[0]));
    }

    /**
     * Test some FileUserManager methods.
     */
    @Test
    public void testSearching() {
        final Set<UserImpl> set = new HashSet<>();
        set.add(new UserImpl(userProfile));
        assertTrue("Username not present", FileUserManager.searchUsername(set, userProfile.getUsername()).isPresent());
        assertTrue("UserProfile not present", FileUserManager.searchUserProfile(set, userProfile).isPresent());
        final Set<UserImpl> emptySet = new HashSet<>();
        assertFalse("Username not present", FileUserManager.searchUsername(emptySet, userProfile.getUsername()).isPresent());
    }

}
