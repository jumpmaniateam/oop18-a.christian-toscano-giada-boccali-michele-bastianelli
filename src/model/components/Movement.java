package model.components;

import org.jbox2d.common.Vec2;

/**
 * Interface to represents a movement component.
 */
public interface Movement extends Component {

    /**
     * Move the entity to the desired direction.
     * 
     * @param movementValue the value to move to.
     */
    void move(Vec2 movementValue);
}
