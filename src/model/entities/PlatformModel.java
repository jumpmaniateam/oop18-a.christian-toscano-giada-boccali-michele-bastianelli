package model.entities;

import enumerators.EntityType;
import enumerators.PlatformType;
import model.physics.PhysicEntity;

/**
 * Model implementation of a platform.
 */
public class PlatformModel extends AbstractEntityModel {

    private static final EntityType TYPE = EntityType.PLATFORM;
    private boolean busy;

    /**
     * @param character the platform character
     * @param physicEntity the physic model of the entity
     */
    public PlatformModel(final PlatformType character, final PhysicEntity physicEntity) {
        super(TYPE, character, physicEntity);
        this.busy = false;
    }
    
    /**
     * Check if the platform is busy by another entity.
     * 
     * @return true if the platform is already busy by another entity
     */
    public boolean isBusy() {
        return busy;
    }

    /**
     * Set the platform busy.
     * 
     * @param busy describes if the platform has an entity on the upside.
     */
    public void setBusy(final boolean busy) {
        this.busy = busy;
    }

}
