package model;

import java.util.Set;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

import controller.entities.Coin;
import controller.entities.Enemy;
import controller.entities.Entity;
import controller.entities.Platform;
import controller.entities.Player;
import enumerators.Level;
import enumerators.PlayerCharacter;

/**
 * Interface for the game model, defines how entities are stored and deleted.
 */
public interface GameModel {

    /**
     * Initializes the JBox2D world. Must be called first.
     * 
     * @param level - level to initialize the game on.
     */
    void init(Level level);

    /**
     * Does a JBox2D step and updates the model.
     */
    void updateWorld();

    /**
     * Updates all entities.
     */
    void updateEntities();

    /**
     * Returns all spawned enemies.
     * 
     * @return enemies set
     */
    Set<Enemy> getEnemiesSet();

    /**
     * Returns the set with coins.
     * 
     * @return the coin set.
     */
    Set<Coin> getCoinSet();

    /**
     * Returns the set with platforms.
     * 
     * @return the platform set
     */
    Set<Platform> getPlatformSet();

    /**
     * @return the topPlatform
     */
    Platform getTopPlatform();

    /**
     * @param topPlatform the topPlatform to set
     */
    void setTopPlatform(Platform topPlatform);

    /**
     * Returns all spawned entities.
     * 
     * @return set with all entities
     */
    Set<Entity> getAllEntities();

    /**
     * Removes the entity at the end of the GameController tick.
     * 
     * @param entity to be removed
     */
    void removeEntityFromMap(Entity entity);

    /**
     * Add the entity to the entities map.
     * 
     * @param entity to be added
     * @param body   of the entity
     */
    void addEntityToMap(Entity entity, Body body);

    /**
     * Returns the entity, given the body.
     * 
     * @param body to get the entity from
     * @return the entity's body
     */
    Entity getEntityFromBody(Body body);

    /**
     * Sets an entity to be destroyed.
     * 
     * @param entity to be destroyed
     */
    void addEntityToDestroy(Entity entity);

    /**
     * Returns the player entity.
     * 
     * @return player entity.
     */
    Player getPlayer();

    /**
     * @return <code>true</code> if the player is dead
     */
    boolean isPlayerDead();

    /**
     * Create the player entity and put it into the entities set.
     * 
     * @param character the player character to create
     * @param position  the position where to create the player
     */
    void createPlayer(PlayerCharacter character, Vec2 position);

    /**
     * Check the entities position and remove them. Also checks if the player is
     * dead.
     */
    void checkEntitiesToDelete();

    /**
     * Put all entities to the collection to be destroyed.
     */
    void cleanAllEntities();

    /**
     * Effectively destroy all the entities that are set to be deleted.
     */
    void deleteEntities();

    /**
     * Clears all the entities maps.
     */
    void resetWorld();

    /**
     * Returns the max height the player has reached.
     * 
     * @return max height
     */
    double getMaxHeight();

}
